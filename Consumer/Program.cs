﻿using System;
using System.Threading;

namespace Consumer
{
    class Program
    {
        static void Main()
        {
            var receiver = new Receiver();
            receiver.MessageListen();           
        }
    }
}
