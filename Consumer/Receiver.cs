﻿using Core;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace Consumer
{
    class Receiver : BrokerMessage
    {               
        public void MessageListen()
        {            
            var subscription = new Subscription(GetModel(), configParams.Queue, false);
            while (true)
            {
                var basicDeliveryEventArgs = subscription.Next();
                var user = JsonSerializer.Deserialize<User>(Encoding.UTF8.GetString(basicDeliveryEventArgs.Body));
                Console.WriteLine(user.ToString());
                if (!string.IsNullOrEmpty(user.Email))
                {                    
                    subscription.Ack(basicDeliveryEventArgs);
                }
            }
        } 
    }
}
