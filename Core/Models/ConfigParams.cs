﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class ConfigParams
    {
        public string HostName { get; set; }
        public string VirtualHost { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Queue { get; set; }
    }
}
