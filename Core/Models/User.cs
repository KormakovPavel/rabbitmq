﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class User
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return $"Имя: {Name}\nВозраст: {Age}\nEmail: {Email}\n";
        }
    }
}
