﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace Core
{
    public class BrokerMessage
    {
        protected readonly ConfigParams configParams;
        public BrokerMessage()
        {
            configParams = JsonSerializer.Deserialize<ConfigParams>(File.ReadAllText("appsettings.json"));
        }
        protected IModel GetModel()
        {
            var model = GetConnection().CreateModel();

            model.QueueDeclare(queue: configParams.Queue,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
            return model;
        }
        private IConnection GetConnection()
        {
            var factory = new ConnectionFactory()
            {
                HostName = configParams.HostName,
                VirtualHost = configParams.VirtualHost,
                UserName = configParams.UserName,
                Password = configParams.Password
            };

            return factory.CreateConnection();
        }

       
    }
}
