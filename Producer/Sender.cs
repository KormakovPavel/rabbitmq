﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading;
using Core;
using RabbitMQ.Client;
using RabbitMQ.Client.MessagePatterns;

namespace Producer
{
    public class Sender : BrokerMessage
    {  
        public void SendForever()
        {
            int id = 0;
            while (true)
            {
                Console.WriteLine(Send(GeneratorUser.Generate(id)));
                Thread.Sleep(1000);
                if (id == 5)
                {
                    id = 0;
                }
                id++;
            }
        }
        private string Send(User user)
        {
            try
            {
                var model = GetModel();
                model.BasicPublish(exchange: "",
                                     routingKey: configParams.Queue,
                                     basicProperties: null,
                                     body: Encoding.UTF8.GetBytes(JsonSerializer.Serialize(user).ToString()));

                return "Сообщение отправлено";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }            
        }
    }
}
