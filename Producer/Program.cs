﻿using System;
using System.Threading;
using System.Text;
using Core;

namespace Producer
{
    class Program
    {
        static void Main()
        {   
            var sender = new Sender();
            sender.SendForever();           
        }
    }
}
